-- phpMyAdmin SQL Dump
-- version 4.0.10deb1ubuntu0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 05, 2020 at 11:36 AM
-- Server version: 5.5.61-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `protect4less`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customerid` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_address` text NOT NULL,
  `customer_email` varchar(255) NOT NULL,
  `customer_phone` char(10) NOT NULL,
  `created` datetime NOT NULL,
  `customer_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customerid`, `customer_name`, `customer_address`, `customer_email`, `customer_phone`, `created`, `customer_status`) VALUES
(1, 'Hundai pre- oned cars', 'Keecheripady , Muvattupuzha.', '', '', '2018-10-01 17:59:29', 0),
(2, 'Rajesh  [Auto Team]', 'ITR road, veloorkunnam, muvattupuzha', '', '9539007171', '2018-10-01 17:58:55', 1),
(3, 'Majestic Car Spa', 'opp: govt: Homeo Hospital\r\nEverest Jn: ,Muvattupuzha', '', '9544984642', '2018-10-04 20:43:33', 1),
(4, 'Jineeh', 'D-Planet', '', '9895316520', '2018-10-02 12:02:37', 1),
(5, 'White Car', 'Kadathi , Muvattupuzha', '', '7034688883', '2018-10-02 12:33:35', 1),
(6, 'Shan Kunnumpuram', 'Kunnumpuram eletricals', '', '9847736649', '2018-10-02 13:56:44', 1),
(7, 'Rins , Riyas', 'Adivad, Kothamangalam', 'rins@gmail.com', '9574852412', '2020-09-05 10:58:06', 1),
(8, 'KAMAR PVM', '', '', '994793662', '2020-09-05 11:11:45', 1),
(9, 'W/S KOTTAYAM ROAD', '', '', '994793662', '2020-09-05 11:11:51', 1),
(10, 'Mahin Kudamunda', 'aliyan', '', '994793662', '2020-09-05 11:11:57', 1),
(11, 'BIKE', 'MAJESTIC CAR SPA', '', '994793662', '2020-09-05 11:12:04', 1),
(12, 'AJI W/S ', 'MARKET BUS STAND\r\nMUVATTUPUZHA', '', '994793662', '2020-09-05 11:12:13', 1),
(13, 'ANEESH TPK', 'CAR DEALER', '', '994793662', '2020-09-05 11:12:34', 1),
(14, 'RIYAS W/S', 'KEECHERIPADY\r\nMUVATTUPUZHA', '', '994793662', '2020-09-05 11:12:41', 1),
(15, 'BINIL BIKE SHOP', '', '', '994793662', '2020-09-05 11:12:49', 1),
(16, 'MAROTTICAL ALIYAN', '', '', '994793662', '2020-09-05 11:12:56', 1),
(17, 'AJAS FRIEND', '', '', '994793662', '2020-09-05 11:13:04', 1),
(18, 'VIKAS', 'MARKET,  MUVATTUPUZHA', '', '994793662', '2020-09-05 11:13:10', 1),
(19, 'Ajims w/s', 'kadathi road.muvattupuzha', '', '994793662', '2020-09-05 11:13:16', 1),
(20, 'APRILIA BIKE', 'MUVATTUPUZHA', '', '994793662', '2020-09-05 11:13:24', 1),
(0, 'sda', '', '', '', '2020-09-05 11:07:45', 0),
(0, 'Shijas', 'Maniyattukudy', '', '994793662', '2020-09-05 11:13:38', 1),
(0, 'Ajas', 'Parekkudy', '', '994793662', '2020-09-05 11:13:55', 1),
(0, 'Ajims', 'Chettukudiyil', '', '994793662', '2020-09-05 11:14:21', 1),
(0, 'Shereef', 'vellakamattom', '', '994793662', '2020-09-05 11:14:34', 1),
(0, 'Jishnu', 'Marottikal', '', '994793662', '2020-09-05 11:14:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_status` int(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`user_id`, `user_name`, `user_password`, `user_status`) VALUES
(1, 'admin', 'e1b4755403710e0deb7aa5d45e43996d', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
