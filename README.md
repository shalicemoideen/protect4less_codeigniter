# protect4less_codeigniter

Codeigniter sample project for CRUD operations with paginations without reload

# Example Application

Contributors: Shalice Moideen 
Requires at least: CodeIgniter 3.0.0 
Tested up to: CodeIgniter 3.0.0 
Version: 1.0  
License: GNU General Public License v2 or later  
License URI: [http://www.gnu.org/licenses/gpl-2.0.html](http://www.gnu.org/licenses/gpl-2.0.html)

---

## Description

This is a sample project done with CodeIgniter for CRUD operations with paginations using Jquery Ajax method(DataTables).

## Features

Includes the following features:

* Custmer management
* User Authentication
* User Sessions
* A specific start page and navigation when a user is not logged into the application
* A specific start page and navigation when a user logs into the application





## Installation

1. Install Codeigniter on your server
2. Place code files from this project within the Codeigniter's location on your server.
3. Create the mysql database **protect4less**, import the database backup from the location **(db/protect4less.sql)**
4. Modify the application/config/database.php file to utilize the database and database user created in step 3.
 **Demo User details,
 username - admin
 password - admin@123#**
5. Modify the application/config/config.php file to utilize the application. 
6. Modify the application/config/config.php file in the following sections:
6a. $config['base_url'] - The base URL for the application  
6b. $config['application_name'] - The name of the application  

7. Attached 3 page sample screenshot in the folder Screenshots


### 1.0

* Initial release
* Released: September 5, 2020


