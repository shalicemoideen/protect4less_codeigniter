<?php
$this->load->view('template/header');
$this->load->view('template/left_navigation');
$this->load->view($body);
$this->load->view('template/footer');
if(isset($script) && $script != '') {
	$this->load->view($script);
}

?>