

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Customer Form
        <!-- <small>Optional description</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

     <!-- Main content -->
    <section class="content">
      <div class="row">

          <!-- right column -->
        <div class="col-md-10">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Customer Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="<?php echo base_url();?>index.php/customer/add">
              
              <div class="box-body">
                <div class="form-group" style="color: red;">
                  <?php if($this->session->flashdata('errors')){ var_dump($this->session->flashdata('errors'));} ?>
                </div>
                <div class="form-group">
                  <label for="customer_name" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                  
                    <input type="text" class="form-control" name="customer_name" id="customer_name" value="<?php if(isset($records->customer_name)) echo $records->customer_name ?>" placeholder="Name">
                    <input type="hidden" class="form-control" name="customer_id" id="customer_id" value="<?php if(isset($records->customerid)) echo $records->customerid ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="customer_address" class="col-sm-2 control-label">Address</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" id="customer_address" name="customer_address"><?php if(isset($records->customer_address)) echo $records->customer_address ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="customer_phone" class="col-sm-2 control-label">Phone</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="customer_phone" id="customer_phone" placeholder="Phone" value="<?php if(isset($records->customer_phone)) echo $records->customer_phone ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="customer_email" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="email" class="form-control" name="customer_email" id="customer_email" placeholder="Email" value="<?php if(isset($records->customer_email)) echo $records->customer_email ?>">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info pull-right">Next</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
        </div>
        <!--/.col (right) -->
     </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->






