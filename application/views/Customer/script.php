<script>

  var response = $("#response").val();
  if(response){
      console.log(response,'response');
      var options = $.parseJSON(response);
      noty(options);
  }
  var param = '';
  var $customerList=[ {'columnName':'customer_name','label':'Customer'} ];
  $(function () {

    var enquiry_type = {'J':'Job','C':'Complaint','F':'Follow-up'};
    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Date picker
    $('#date').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy'
    });


     //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });

     //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    $table = $('#customer_table').DataTable( {
        "processing": true,
        "serverSide": true,
        
        "bDestroy" : true,
        "ajax": {
            "url": "<?php echo base_url();?>index.php/customer/get/",
            "type": "POST",
            "data" : function (d) {
            
           }
        },
        "createdRow": function ( row, data, index ) {
          
            $('td',row).eq(0).html(index+1);
            $('td', row).eq(3).html('<center><a href="<?php echo base_url();?>index.php/customer/edit/'+data['customerid']+'"><i class="fa fa-edit iconFontSize-medium" ></i></a> &nbsp;&nbsp;&nbsp;<a onclick="return confirmDelete('+data['customerid']+')"><i class="fa fa-trash-o iconFontSize-medium" ></i></a></center>');
            
        },

        "columns": [
            { "data": "customer_name" ,"orderable": false },
            { "data": "customer_name" ,"orderable": true },
            { "data": "customer_phone", "orderable": false },
            { "data": "customer_phone", "orderable": false }
            
        ]
        
    } );


    
    
  });

  function confirmDelete(customer_id){
    var conf = confirm("Do you want to delete Customer ?");
    if(conf){
        $.ajax({
            url:"<?php echo base_url();?>index.php/customer/delete",
            data:{customer_id:customer_id},
            method:"POST",
            datatype:"json",
            success:function(data){
                var options = $.parseJSON(data);
                noty(options);
                $table.ajax.reload();
            }
        });

    }
    else{
        return false;
    }

}
</script>