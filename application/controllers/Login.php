<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('Loginmodel');
    }
    public function index(){
        $this->form_validation->set_rules('username', 'Username', 'required');
        if ($this->form_validation->run() == FALSE) {
              $this->load->view('login');
        } else {
                $data = array('user_name' => $this->input->post('username'),
                              'user_password' => md5($this->input->post('password'))
                              );
                // var_dump($data);exit();
                $result = $this->Loginmodel->checkUserLogin($data);
                if($result){
                    redirect('/customer/');
                }
                else{
                    $error['message'] = "The user name or password is invalid";
                    $this->load->view('login',$error);
                }
        }
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect('/login/');
    }
}
?>
