<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {
	public $enquiry_table = 'enquiry';
	public $customer_table = 'customer';
	public $page  = 'customer';
	public function __construct() {
		parent::__construct();
        if(! $this->is_logged_in()){
            redirect('/login');
        }
        
        $this->load->model('General_model');
	}
	public function index()
	{
		$template['body'] = 'Customer/list';
		$template['script'] = 'Customer/script';
		$this->load->view('template', $template);
	}
	public function add(){

		$this->form_validation->set_rules('customer_email', 'Email', 'valid_email');
        $this->form_validation->set_rules('customer_name', 'Name', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('errors', validation_errors());
        	$template['enquiry_type'] = $this->config->item('enquiry_type');
            $template['body'] = 'Customer/add';
			$template['script'] = 'Customer/script';
			$this->load->view('template', $template);
        } else {
        	$customer_data = array(
                        'customer_name' => $this->input->post('customer_name'),
                        'customer_address' => $this->input->post('customer_address'),
                        'customer_phone' => $this->input->post('customer_phone'),
                        'customer_email' => $this->input->post('customer_email'),
                        'created' => date('Y-m-d H:i:s'),
                        'customer_status' => 1
                        );
        	$customer_id = $this->input->post('customer_id');
        	if($customer_id) {
        		$result = $this->General_model->update($this->customer_table,$customer_data,'customerid',$customer_id);
        		$response_text = "Customer updated successfully";
        	}
        	else {
        		$result = $this->General_model->add($this->customer_table,$customer_data);
        		$response_text = "Customer added successfully";
        	}
        	
	        if($result){
	            $this->session->set_flashdata('response', "{&quot;text&quot;:&quot;$response_text&quot;,&quot;layout&quot;:&quot;topRight&quot;,&quot;type&quot;:&quot;success&quot;}");
	        }
	        else{
	            $this->session->set_flashdata('response', '{&quot;text&quot;:&quot;Something went wrong,please try again later&quot;,&quot;layout&quot;:&quot;bottomRight&quot;,&quot;type&quot;:&quot;error&quot;}');
	        }
	        redirect('/customer/', 'refresh');
	    }
	}
	public function edit($customer_id){
    	$template['page'] = $this->page;
    	$template['records'] = $this->General_model->get_row($this->customer_table,'customerid',$customer_id);
    	$template['body'] = 'Customer/add';
		$template['script'] = 'Customer/script';
		$this->load->view('template', $template);
    }
    public function delete(){
        $customer_id = $this->input->post('customer_id');
        $updateData = array('customer_status' => 0);
        $data = $this->General_model->update($this->customer_table,$updateData,'customerid',$customer_id);
        if($data) {
            $response['text'] = 'Deleted successfully';
            $response['type'] = 'success';
        }
        else{
            $response['text'] = 'Something went wrong';
            $response['type'] = 'error';
        }
        $response['layout'] = 'topRight';
        $data_json = json_encode($response);
        echo $data_json;
    }

	public function get(){
		$this->load->model('Customer_model');
    	$param['draw'] = (isset($_REQUEST['draw']))?$_REQUEST['draw']:'';
        $param['length'] =(isset($_REQUEST['length']))?$_REQUEST['length']:'10'; 
        $param['start'] = (isset($_REQUEST['start']))?$_REQUEST['start']:'0';
        $param['order'] = (isset($_REQUEST['order'][0]['column']))?$_REQUEST['order'][0]['column']:'';
        $param['dir'] = (isset($_REQUEST['order'][0]['dir']))?$_REQUEST['order'][0]['dir']:'';
        $param['searchValue'] =(isset($_REQUEST['search']['value']))?$_REQUEST['search']['value']:'';
        
    	$data = $this->Customer_model->getCustomerTable($param);
    	$json_data = json_encode($data);
    	echo $json_data;
    }
}
