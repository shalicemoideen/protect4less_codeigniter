<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customer_model extends CI_Model{

	public function __construct()
    {
        parent::__construct();
    }
    public function getCustomerTable($param){
		$arOrder = array('','customer_name');
		$searchValue =($param['searchValue'])?$param['searchValue']:'';
        if($searchValue){
            $this->db->like('customer_name', $searchValue); 
        }
        $this->db->where("customer_status",1);

        if ($param['order'] != 'false' and $param['dir'] != 'false') {
            $order_field = $arOrder[$param['order']];
            $this->db->order_by($order_field,$param['dir']);
        }
        if ($param['start'] != 'false' and $param['length'] != 'false') {
        	$this->db->limit($param['length'],$param['start']);
        }

        $this->db->select('*');
        $this->db->from('customer');
        $query = $this->db->get();
        // echo $this->db->last_query();

        $data['data'] = $query->result();
        $data['recordsTotal'] = $this->getCustomerTotalCount($param);
        $data['recordsFiltered'] = $this->getCustomerTotalCount($param);
        return $data;

	}

	public function getCustomerTotalCount($param = NULL){

		$searchValue =($param['searchValue'])?$param['searchValue']:'';
        if($searchValue){
            $this->db->like('customer_name', $searchValue); 
        }
        $this->db->where("customer_status",1);
        $this->db->select('customerid');
        $this->db->from('customer');
        $query = $this->db->get();
    	return $query->num_rows();
    }
}
?>